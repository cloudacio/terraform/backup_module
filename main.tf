resource "aws_backup_vault" "default" {
  name = var.vault_name
}

resource "aws_backup_plan" "default" {
  name = var.plan_name

  rule {
    rule_name                = var.rule_name
    target_vault_name        = aws_backup_vault.default.name
    schedule                 = var.schedule
    start_window             = var.start_window
    completion_window        = var.completion_window
    enable_continuous_backup = var.enable_continuous_backup

    lifecycle {
      cold_storage_after = var.cold_storage_after
      delete_after       = var.delete_after
    }
  }
}

data "aws_iam_policy_document" "assume_role" {
  count = var.create_iam_role ? 1 : 0
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "default" {
  count              = var.create_iam_role ? 1 : 0
  name               = "AWSBackupServiceRolePolicyForBackup"
  assume_role_policy = join("", data.aws_iam_policy_document.assume_role.*.json)
}

data "aws_iam_role" "existing" {
  count = var.create_iam_role == false ? 1 : 0
  name  = "AWSBackupServiceRolePolicyForBackup"
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = var.create_iam_role ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = join("", aws_iam_role.default.*.name)
}

resource "aws_backup_selection" "default" {
  name         = var.selection_name
  iam_role_arn = join("", var.create_iam_role ? aws_iam_role.default.*.arn : data.aws_iam_role.existing.*.arn)
  plan_id      = aws_backup_plan.default.id
  resources    = var.backup_resources
}
