# Module for creation of AWS Backups

This module will create: 

- AWS Backup Vault
- AWS Backup Plan
- AWS Backup Rule

### Usage

```
module "rds_backup_example1" {
  source                   = "git::ssh://git@gitlab.com/cloudacio/terraform/backup_module.git?ref=v<tag>"
  completion_window        = 120
  create_iam_role          = true
  enable_continuous_backup = true
  delete_after             = 7
  plan_name                = "rds_backup_plan"
  rule_name                = "rds_backup_rule"
  schedule                 = "cron(0 12 * * ? *)"
  selection_name           = "rds_backup"
  start_window             = 60
  vault_name               = "rds_backup_vault"

  backup_resources = [
    module.rds_example1.instance_arn
  ]
}

module "efs_backup_example1" {
  source                   = "git::ssh://git@gitlab.com/cloudacio/terraform/backup_module.git?ref=v<tag>"
  depends_on               = [module.rds_backup_example1]
  completion_window        = 120
  create_iam_role          = false
  enable_continuous_backup = false
  delete_after             = 7
  plan_name                = "efs_backup_plan"
  rule_name                = "efs_backup_rule"
  schedule                 = "cron(0 1 * * ? *)"
  selection_name           = "efs_backup"
  start_window             = 60
  vault_name               = "efs_backup_vault"

  backup_resources = [
    aws_efs_file_system.efs_example1.arn
  ]
}

### Notes

- enable_continuous_backup only works for RDS backups
- If you have multiple modules (for diff resources) only need to enable create_iam_role once but 
  will need to depend on the first module (the one that creates the iam role)
